import os
from socket import create_connection
import unittest
from selenium.webdriver.chrome.options import Options
from unittest.main import main
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

os.makedirs('results')
download_path = os.getcwd()+'/results'
class SubmitNewClique(unittest.TestCase):

    

    def setUp(self):

        print(download_path)
        chromeOptions = Options()
        chromeOptions.add_experimental_option("prefs",
        {"download.default_directory": download_path,})

        self.driver = webdriver.Chrome(executable_path= r'/Users/sigmotoa/Documents/Data/Platzi/selenium/drivers/chromedriver', chrome_options=chromeOptions)
        driver = self.driver
        
        driver.maximize_window()
        driver.implicitly_wait(30)
        driver.get('https://www.ncbi.nlm.nih.gov/Structure/bwrpsb/bwrpsb.cgi')

    def test_new_clique(self):
        driver = self.driver
        job_title = driver.find_element_by_name('customid')
        email = driver.find_element_by_name('notify')
        attach_file = driver.find_element_by_xpath('//*[@id="frm_New_Search"]/div/table/tbody/tr/td/table/tbody/tr[1]/td[1]/div[4]/input')
        submit_button = driver.find_element_by_xpath('//*[@id="frm_New_Search"]/div/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[3]/input')

        job_title.clear()
        email.clear()

        

        self.assertTrue(job_title.is_enabled()
        and email.is_enabled()
        and attach_file.is_enabled())

        
        job_title.send_keys('Test2')#Poner código del batch
      
        email.send_keys('sigmotoa@misena.edu.co')
        
        #sleep(5)
        attach_file.send_keys('/Users/sigmotoa/OneDrive - Universidad Autónoma de Aguascalientes/ProyectoFinal/Codes/batches_2/batches/cliques_006_batch_clique0.faa')
        #sleep(5)
        #window = driver.switch_to_active_element
                
        submit_button.click()

        
        succesfull = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.ID, 'div_Status')))
        succesfull_text = driver.find_element_by_id('div_Status')
        self.assertEqual('Search completed successfully', succesfull_text.text)

        concise = driver.find_element_by_id('hid_dmode_rep')
        concise.click()
        
        
        print(concise.is_selected())
        download = driver.find_element_by_css_selector('#tbl_DLPanel > tbody > tr:nth-child(3) > td:nth-child(3) > input[type=image]')
        download.click()
        finished = WebDriverWait(self.driver, 3).until(EC.visibility_of_element_located((By.ID, 'td_XmpTable')))
        sleep(4)
        #self.driver.implicitly_wait(3000)
        
        


    def tearDown(self):
        #self.driver.implicitly_wait(3)
        self.driver.close()

if __name__ == "__main__":
    unittest.main(verbosity=2)