import unittest
from pyunitreport import HTMLTestRunner
from selenium import webdriver


class HelloWorld(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path= r'/Users/sigmotoa/Documents/Data/Platzi/selenium/drivers/chromedriver')
        driver = cls.driver
        driver.implicitly_wait(5)

    def test_hello_world(self):
        driver = self.driver
        driver.get('https://www.platzi.com')

    def test_visit_wiki(self):
        driver = self.driver
        driver.get('https://www.wikipedia.org')

    def test_cdd(self):
        driver = self.driver
        driver.get('https://www.ncbi.nlm.nih.gov/Structure/bwrpsb/bwrpsb.cgi')

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == "__main__":
    unittest.main(verbosity = 2, testRunner = HTMLTestRunner(output = '/Users/sigmotoa/Documents/Data/Platzi/selenium/reportes', report_name = 'hello-world-report'))